const app = Vue.createApp({
  data() {
    return {
      currentUserInput: '',
      message: 'Vue is great!',
    };
  },
  methods: {
    saveInput(event) {
      console.log('fire');
      this.currentUserInput = event.target.value;
    },
    setText() {
      // this.message = this.currentUserInput;
      this.message = this.$refs.usertext.value + this.$refs.usertext2.value;
    },
  },
  created(){
    console.log('created()');
    console.log(this.message);
  },
  beforeCreate(){
    console.log('beforCreate()');
    console.log(this.message);
  },
  mounted(){
    console.log('mounted()');
    console.log(this.message);
  },
  beforeMount(){
    console.log('beforeMount()');
    console.log(this.message);
  },
  updated(){
    console.log('updated()');
    console.log(this.message);
  },
  beforeUpdate(){
    console.log('beforeUpdate()');
    console.log(this.message);
  },
  unmounted(){
    console.log('unmounted()');
    console.log(this.message);

  },
  beforeUnmount(){
    console.log('beforeUnmount()');
    console.log(this.message);
  },
});

app.mount('#app');

setTimeout(function(){
  app.unmount();
},5000);
